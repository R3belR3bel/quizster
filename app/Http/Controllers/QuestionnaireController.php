<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Question;
use App\Questionnaire;

class QuestionnaireController extends Controller
{

    /*
   * Secure the set of pages to the user.
   */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {

        $questionnaires = Questionnaire::all();

        return view('user/questionnaires', ['questionnaires' => $questionnaires]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $qs = Questionnaire::lists('title', 'id');

        // now we can return the data with the view
        return view('user/questionnaires/create', compact('qs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $questionnaire = Questionnaire::create($request->all());

        return redirect('user/questionnaires');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // get the questionnaires
        $questionnaire = Questionnaire::where('id',$id)->first();

        // if questionnaires does not exist return to list
        if(!$questionnaire)
        {
            return redirect('/user/questionnaires'); // you could add on here the flash messaging of questionnaires does not exist.
        }
        return view('/user/questionnaires/show')->withQuestionnaire($questionnaire);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
