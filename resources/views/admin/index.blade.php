<!doctype html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>All Users</title>
</head>
<body>
<h1>All Users</h1>

<section>
    @if (isset ($users))

        <table>
            <tr>
                <th>Username</th>
                <th>email</th>
                <th>Permissions</th>
            </tr>
            @foreach ($users as $user)
                <tr>
                    <td><a href="/admin/users/{{ $user->id }}" name="{{ $user->name }}">{{ $user->name }}</a></td>
                    <td> {{ $user->email }}</td>
                    <td></td>
                </tr>
            @endforeach
        </table>
    @else
        <p>no users</p>
    @endif
</section>

</body>
</html>